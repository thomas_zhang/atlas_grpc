FROM docker-registry.owler.internal/owler/docker-base:0.2
RUN apt-get update
RUN apt-get install -y python
RUN apt-get install -y python-pip
RUN pip install grpcio python-igraph numpy grpcio-tools nltk


COPY ./deploy/ /home/app/deploy/

COPY ./deploy/scripts/ /home/app/scripts/

RUN mkdir -p /home/app/installs/atlas_grpc/ && mkdir -p /home/app/installs/atlas_grpc/src

COPY ./src/ /home/app/installs/atlas_grpc/src

COPY ./deploy/supervisor/ /etc/supervisor/conf.d/
COPY ./deploy/filebeat/ /etc/filebeat/conf.d/
