"""
main script for ATLAS clustering. For full functionality refer to private atlas repository.
"""
import json
import sys
import igraph as ig
import numpy as np
import logging

"""
layer one: graph expansion and building.
"""
def build_graph(nodes, edges, threshold = 1000):
    """ script for building networks. building igraph file base on the input json formats."""
    # add nodes
    N = len(nodes)
    ndic = {}
    for i,node in enumerate(nodes):
        ndic[node.CompanyId] = i

    # add edges
    slist = []
    tlist = []
    wlist = []
    for i,edge in enumerate(edges):
        slist.append(ndic[edge.SourceCompanyId])
        tlist.append(ndic[edge.DestCompanyId])
        wlist.append(edge.ProximityScore)
    elist = zip(slist,tlist)

    # build graph.
    g = ig.Graph(directed = False)
    g.add_vertices(N)
    g.add_edges(elist)
    g.simplify()
    g.es['weight'] = wlist
    return g, ndic


"""
layer two: clustering and community detection.
"""
def louvain(g,level = 'min'):
    glevels = g.community_multilevel(return_levels = True)
    if level == 'max':
        glist = glevels[-1].membership
    elif level == 'min':
        glist = glevels[0].membership
    else:
        logging.error('only "max" and "min" arguments are guaranteed and supported at the moment.')
        return
    g.vs['group'] = glist
    return g


"""
layer three: ranking/centarlity measure. NOT USING/IMPLEMENTED AT THE MOMENT.
"""


"""
overall main.
"""
def atlas_compute(nodes,edges):
    logging.info('clustering begins')
    g, ndic = build_graph(nodes, edges)
    logging.info('clustering ends')
    grouped_g = louvain(g)
    logging.info('id mapped to group number')
    # id mapping
    gdic = {}
    for k,v in ndic.iteritems():
        gdic[k] = grouped_g.vs['group'][v]
    logging.info('computation ends.')
    return gdic
