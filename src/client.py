import grpc

from protobuf.atlas_pb2 import ClusteredCompetitiveGraphResponse, GroupedNode, Node, Edge, CompetitiveGraphRequest
from protobuf.atlas_pb2_grpc import AtlasServiceStub

# open a gRPC channel
channel = grpc.insecure_channel('localhost:50051')

# create a stub (client)
stub = AtlasServiceStub(channel)

# create a valid request message
node1 = Node(CompanyId = 100000)
node2 = Node(CompanyId = 100001)
edge = Edge(SourceCompanyId = 100000, DestCompanyId = 100001, ProximityScore = 5000)
request = CompetitiveGraphRequest(Nodes = [node1,node2], Edges = [edge])

# make the call
response = stub.clusterGraph(request)
