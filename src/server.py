"""
Sever side of the the grpc code.
"""

import grpc
import time
from concurrent import futures
from functions.func_main import atlas_compute
from protobuf.atlas_pb2 import ClusteredCompetitiveGraphResponse, GroupedNode
from protobuf.atlas_pb2_grpc import add_AtlasServiceServicer_to_server
import logging
import sys

logging.basicConfig(stream=sys.stdout, level=logging.INFO)

class AtlasClusteringService():
    # ATLAS clustering service.
    def clusterGraph(self, request, context):
        gdic = atlas_compute(request.Nodes, request.Edges)
        results = []
        for k,v in gdic.iteritems():
            a = GroupedNode(CompanyId = k, CompanyGroup = v+1)
            results.append(a)
        response = ClusteredCompetitiveGraphResponse(Nodes = results)
        return response


def start_server():
    logging.info('starting server...')
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    add_AtlasServiceServicer_to_server(
        AtlasClusteringService(), server)

    # start server
    logging.info('server stared, hosting at port localhost:50051')
    server.add_insecure_port('localhost:50051')
    server.start()

    logging.info('started server')
    logging.info('sleeping')
    # keep server alive
    try:
        while True:
            time.sleep(60 * 60 * 24)
    except KeyboardInterrupt:
        server.stop(0)


if __name__ == '__main__':
    start_server()
